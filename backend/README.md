## Description

Simple API with faucet for COIN token deployed on blockchain.

Exposes endpoints:

```
GET /faucet/balance
```

```
GET /faucet/balance/:address
```

```
POST /faucet/transfer
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# Define private key of coin contract deployer
expose PRIVATE_KEY="0x..."

# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```