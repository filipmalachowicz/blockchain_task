import { Module } from '@nestjs/common';
import { FaucetController } from './faucet.controller';
import { FaucetService } from './faucet.service';
import { ContractsModule } from '../contracts/contracts.module';
import { ConfigurationModule } from '../configuration/configuration.module';

@Module({
    imports: [ContractsModule, ConfigurationModule],
    controllers: [FaucetController],
    providers: [FaucetService],
})
export class FaucetModule {}
