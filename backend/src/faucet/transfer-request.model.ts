import { BigNumber } from 'ethers/utils';

export class TransferRequest {
    constructor(readonly address: string, readonly amount: BigNumber) {}
}
