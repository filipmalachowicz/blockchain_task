export class BalanceResponse {
    constructor(readonly balance: number, readonly address: string) {}
}
