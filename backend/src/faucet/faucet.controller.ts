import { Controller, Get, Post, HttpCode, Body, Param } from '@nestjs/common';
import { FaucetService } from './faucet.service';
import { ethers } from 'ethers';
import { TransferRequest } from './transfer-request.model';
import { BalanceResponse } from './balance-response.model';
import { TransferResponse } from './transfer-response.model';

@Controller('faucet')
export class FaucetController {
    constructor(private readonly faucetService: FaucetService) {}

    @Get('balance')
    async getFaucetBalance() {
        const balanceResult = await this.faucetService.getFaucetBalance();

        return new BalanceResponse(balanceResult.balance.toNumber(), balanceResult.address);
    }

    @Get('balance/:address')
    async getBalance(@Param('address') address: string) {
        const checksumedAddress = ethers.utils.getAddress(address);
        const balanceResult = await this.faucetService.getAccountBalance(checksumedAddress);

        return new BalanceResponse(balanceResult.balance.toNumber(), balanceResult.address);
    }

    @Post('transfer')
    @HttpCode(201)
    async transferCoins(@Body('address') address: string, @Body('amount') amount: string) {
        const amountToTransfer = ethers.utils.bigNumberify(amount);
        const checksumedAddress = ethers.utils.getAddress(address);
        const transferRequest = new TransferRequest(checksumedAddress, amountToTransfer);

        const transferResult = await this.faucetService.transferCoinsFromFaucet(transferRequest);

        return new TransferResponse(
            transferResult.transactionHash,
            transferResult.transactionIndex,
            transferResult.blockNumber,
            transferResult.blockHash,
            transferResult.confirmations);
    }
}
