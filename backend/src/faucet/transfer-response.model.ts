export class TransferResponse {
    constructor(
        readonly transactionHash: string,
        readonly transactionIndex: number,
        readonly blockNumber: number,
        readonly blockHash: string,
        readonly confirmations: number,
    ) {}
}
