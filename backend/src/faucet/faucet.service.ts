import { Injectable } from '@nestjs/common';
import { CoinService } from '../contracts/coin.service';
import { TransferRequest } from './transfer-request.model';

@Injectable()
export class FaucetService {
    constructor(private readonly coinService: CoinService) {}

    async getFaucetBalance() {
        return await this.coinService.getFaucetBalance();
    }

    async transferCoinsFromFaucet(transferRequest: TransferRequest) {
        return await this.coinService.sendTokens(transferRequest.address, transferRequest.amount);
    }

    async getAccountBalance(address: string) {
        return await this.coinService.getBalance(address);
    }
}
