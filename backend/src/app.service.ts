import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getApiInformation(): string {
    return 'COIN Faucet API';
  }
}
