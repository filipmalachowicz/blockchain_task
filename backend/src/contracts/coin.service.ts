import { Injectable } from '@nestjs/common';
import { ConfigurationProviderService } from '../configuration/configuration.service';
import { Coin } from '../../types/ethers-contracts/Coin';
import { CoinFactory } from '../../types/ethers-contracts/CoinFactory';
import { ethers, Wallet } from 'ethers';
import { Provider } from 'ethers/providers';
import { BigNumber } from 'ethers/utils';

@Injectable()
export class CoinService {
    public readonly coin: Coin;
    private readonly provider: Provider;
    private readonly deployersWallet: Wallet;

    constructor(private readonly configurationProvider: ConfigurationProviderService) {
        const deployedContractAddress = configurationProvider.getCoinContractAddress();
        const nodeAddress = configurationProvider.getNodeAddress();
        const deployerPrivateKey = configurationProvider.getDeployerPrivateKey();

        // TODO: Move connections to node and contract as a Nest.js asynchronous provider and handle connections errors there
        this.provider = new ethers.providers.JsonRpcProvider(nodeAddress);
        this.deployersWallet = new Wallet(deployerPrivateKey, this.provider);

        this.coin = CoinFactory.connect(deployedContractAddress, this.deployersWallet);
    }

    async getFaucetBalance() {
        return this.getBalance(this.deployersWallet.address);
    }

    async sendTokens(reciverAddress: string, amountToSend: BigNumber) {
        const currentFaucetBalance = await this.getFaucetBalance();

        if (currentFaucetBalance.balance.lt(amountToSend)) {
            const minimumFaucetBalance = this.configurationProvider.getMinimumBalance();
            const amountToMint = minimumFaucetBalance.add(amountToSend).sub(currentFaucetBalance.balance);
            await this.mintTokens(amountToMint);
        }

        const tx = await this.coin.send(reciverAddress, amountToSend);
        const txResponse = await tx.wait();

        return {
            transactionHash: txResponse.transactionHash,
            transactionIndex: txResponse.transactionIndex,
            blockNumber: txResponse.blockNumber,
            blockHash: txResponse.blockHash,
            confirmations: txResponse.confirmations,
        };
    }

    async getBalance(address: string) {
        const balance = await this.coin.balances(address);

        return { balance, address };
    }

    private async mintTokens(numberOfNewTokens: BigNumber) {
        const tx = await this.coin.mint(this.deployersWallet.address, numberOfNewTokens);
        const txResponse = await tx.wait();

        return {
            transactionHash: txResponse.transactionHash,
            transactionIndex: txResponse.transactionIndex,
            blockNumber: txResponse.blockNumber,
            blockHash: txResponse.blockHash,
            confirmations: txResponse.confirmations,
        };
    }
}
