import { Module } from '@nestjs/common';
import { CoinService } from './coin.service';
import {ConfigurationModule} from '../configuration/configuration.module';

@Module({
    imports: [ConfigurationModule],
    providers: [CoinService],
    exports: [CoinService],
})
export class ContractsModule {}
