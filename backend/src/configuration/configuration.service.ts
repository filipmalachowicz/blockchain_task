import { Injectable } from '@nestjs/common';
import { BigNumber } from 'ethers/utils';
import { ethers } from 'ethers';

@Injectable()
export class ConfigurationProviderService {
    getMinimumBalance(): BigNumber {
        const minimumBalance = process.env.MINIMUM_BALANCE || 10;
        return ethers.utils.bigNumberify(minimumBalance);
    }
    getNodeAddress(): string {
        const nodeUri = process.env.NODE_URI;
        if (!nodeUri) {
            return 'http://localhost:8545/';
        }
        return nodeUri;
    }

    getFaucetAddress(): string {
        return '0x3f85D0b6119B38b7E6B119F7550290fec4BE0e3c';
    }

    getCoinContractAddress(): string {
        return '0xb4c79daB8f259C7Aee6E5b2Aa729821864227e84';
    }

    getDeployerPrivateKey(): string {
        const privateKey = process.env.PRIVATE_KEY;
        if (!privateKey) {
            throw new Error('Environment variable PRIVATE_KEY is not defined');
        }
        return privateKey;
    }
}
