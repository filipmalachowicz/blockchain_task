import { Module } from '@nestjs/common';
import { ConfigurationProviderService } from './configuration.service';

@Module({
    providers: [ConfigurationProviderService],
    exports: [ConfigurationProviderService],
})
export class ConfigurationModule {}
